/**
 * Created by oboiko2 on 12/13/19.
 */

trigger InterviewTrigger on Interview__c (before update) {
    InterviewTriggerHandler.run(Trigger.New);
}