/**
 * Created by oboiko2 on 12/16/19.
 */

@IsTest
private class InterviewTriggerHandlerTest extends testUtils{
    static testMethod void testCloseManyInterviews(){
        Map<String, SObject[]> setupData = setup(
                5,          // contactsCount,
                200,        // interviewCount,
                2,          // feedbackDaysAgo,
                0,          // oldFeedbacksCount,
                3,          // newFeedbacksCount
                3           // completedFB
        );
        Vacancy__c vacancy = (Vacancy__c) setupData.get('vacancies')[0];
        Interview__c[] interviews = (Interview__c[]) setupData.get('interviews');
        Contact[] contacts = (Contact[]) setupData.get('contacts');
        FeedBack__c[] feedBacks = (FeedBack__c[]) setupData.get('feedbacks');
        FeedBack__c[] newFeedBacks = new List<FeedBack__c>();

        for(Integer i = 0; i < interviews.size(); i++) {
            for (Integer j = 0; j < 3; j++) {
                Contact c = contacts[Math.mod(i+j, 5)];
                FeedBack__c fb = new FeedBack__c(
                        Interview__c=interviews[i].id,
                        Interviewer__c=c.id,
                        WritableDateTime__c = Datetime.now(),
                        English_score__c = '9',
                        Hard_skills_level__c = 'Senior',
                        Hard_Skills_Score__c = '10',
                        Soft_skills_score__c = '9'
                );
                newFeedBacks.add(fb);
            }
        }

        insert newFeedBacks;

        Interview__c[] upsertInterviews = new List<Interview__c>();
        for (Interview__c interview: [select id, Candidate_hired__c from Interview__c]) {
            interview.Candidate_hired__c = true;
            upsertInterviews.add(interview);
        }

        Test.startTest();
        DmlException err;
        try {
            upsert upsertInterviews;
        } catch (DmlException e) {
            err = e;
        }
        System.assert(err == null);

        Test.stopTest();
        for (Vacancy__c vac : [select id, Closed_Date__c, Status__c, Approved_candidate__c from Vacancy__c]){
            System.assert(vac.Closed_Date__c != null);
            System.assert(vac.Status__c == 'Closed');
        }
    }
    static testMethod void testCloseNotCompletedInterview(){
        Map<String, SObject[]> setupData = setup(
                5,          // contactsCount,
                1,          // interviewCount,
                2,          // feedbackDaysAgo,
                0,          // oldFeedbacksCount,
                0,          // newFeedbacksCount,
                3           // completedFeedbacks
        );
        Vacancy__c vacancy = (Vacancy__c) setupData.get('vacancies')[0];
        Interview__c interview = (Interview__c) setupData.get('interviews')[0];
        Contact[] contacts = (Contact[]) setupData.get('contacts');
        FeedBack__c[] feedBacks = (FeedBack__c[]) setupData.get('feedbacks');

        Interview__c[] upsertInterviews = new List<Interview__c>();
        for(Interview__c inter : (Interview__c[]) setupData.get('interviews')) {
            inter.isClosed__c = true;
            upsertInterviews.add(inter);
        }

        DmlException err;
        try {
            upsert upsertInterviews;
        } catch (DmlException e) {
            err = e;
        }
        System.assert(err != null && err.getMessage().contains('Could not close not completed interview!'));
    }

    static testMethod void testCloseCompletedInterviewNoCompletedFeedbacks(){
        Map<String, SObject[]> setupData = setup(
                5,          // contactsCount,
                1,          // interviewCount,
                2,          // feedbackDaysAgo,
                0,          // oldFeedbacksCount,
                3,          // newFeedbacksCount,
                0           // completedFeedbacks
        );
        Vacancy__c vacancy = (Vacancy__c) setupData.get('vacancies')[0];
        Interview__c interview = (Interview__c) setupData.get('interviews')[0];
        Contact[] contacts = (Contact[]) setupData.get('contacts');
        FeedBack__c[] feedBacks = (FeedBack__c[]) setupData.get('feedbacks');

        Interview__c[] upsertInterviews = new List<Interview__c>();

        for(Interview__c inter : (Interview__c[]) setupData.get('interviews')) {
            inter.Completed__c = true;
            upsertInterviews.add(inter);
        }

        update upsertInterviews;

        upsertInterviews = new List<Interview__c>();
        for(Interview__c inter : (Interview__c[]) setupData.get('interviews')) {
            inter.isClosed__c = true;
            upsertInterviews.add(inter);
        }

        DmlException err;
        try {
            upsert upsertInterviews;
        } catch (DmlException e) {
            err = e;
        }
        System.assert(err != null && err.getMessage().contains('Could not close interview until it has 3 completed Feedbacks.'));
    }

    static testMethod void testCloseCompletedInterviewWithCompletedFeedbacks(){
        Map<String, SObject[]> setupData = setup(
                5,          // contactsCount,
                1,          // interviewCount,
                2,          // feedbackDaysAgo,
                0,          // oldFeedbacksCount,
                1,          // newFeedbacksCount,
                3           // completedFeedbacks
        );
        Vacancy__c vacancy = (Vacancy__c) setupData.get('vacancies')[0];
        Interview__c interview = (Interview__c) setupData.get('interviews')[0];
        Contact[] contacts = (Contact[]) setupData.get('contacts');
        FeedBack__c[] feedBacks = (FeedBack__c[]) setupData.get('feedbacks');

        Interview__c[] upsertInterviews = new List<Interview__c>();

        for(Interview__c inter : (Interview__c[]) setupData.get('interviews')) {
            inter.Completed__c = true;
            upsertInterviews.add(inter);
        }

        update upsertInterviews;

        upsertInterviews = new List<Interview__c>();
        for(Interview__c inter : (Interview__c[]) setupData.get('interviews')) {
            inter.isClosed__c = true;
            upsertInterviews.add(inter);
        }

        DmlException err;
        try {
            upsert upsertInterviews;
        } catch (DmlException e) {
            err = e;
        }
        System.assert(err == null);
    }

    static testMethod void testVacancyUpdatedWhenCloseInterview() {
        Map<String, SObject[]> setupData = setup(
                5,          // contactsCount,
                1,          // interviewCount,
                2,          // feedbackDaysAgo,
                0,          // oldFeedbacksCount,
                1,          // newFeedbacksCount,
                3           // completedFeedbacks
        );
        Vacancy__c vacancy = (Vacancy__c) setupData.get('vacancies')[0];
        Interview__c interview = (Interview__c) setupData.get('interviews')[0];
        Contact[] contacts = (Contact[]) setupData.get('contacts');
        FeedBack__c[] feedBacks = (FeedBack__c[]) setupData.get('feedbacks');

        Interview__c[] upsertInterviews = new List<Interview__c>();

        for (Interview__c inter : (Interview__c[]) setupData.get('interviews')) {
            inter.Completed__c = true;
            upsertInterviews.add(inter);
        }

        update upsertInterviews;

        upsertInterviews = new List<Interview__c>();
        for (Interview__c inter : (Interview__c[]) setupData.get('interviews')) {
            inter.isClosed__c = true;
            upsertInterviews.add(inter);
        }

        DmlException err;
        try {
            upsert upsertInterviews;
        } catch (DmlException e) {
            err = e;
        }
        System.assert(err == null);

        for (Vacancy__c vac : [select id, Closed_Date__c, Status__c, Approved_candidate__c from Vacancy__c]) {
            System.assertNotEquals(vac.Closed_Date__c, null);
            System.assertEquals(vac.Status__c, 'Closed');
            System.assertEquals(vac.Approved_candidate__c, null);
        }
    }


    static testMethod void testVacancyUpdatedWhenCloseInterviewWithHiredCandidate() {
        Map<String, SObject[]> setupData = setup(
                5,          // contactsCount,
                1,          // interviewCount,
                2,          // feedbackDaysAgo,
                0,          // oldFeedbacksCount,
                1,          // newFeedbacksCount,
                3           // completedFeedbacks
        );
        Vacancy__c vacancy = (Vacancy__c) setupData.get('vacancies')[0];
        Interview__c interview = (Interview__c) setupData.get('interviews')[0];
        Contact[] contacts = (Contact[]) setupData.get('contacts');
        FeedBack__c[] feedBacks = (FeedBack__c[]) setupData.get('feedbacks');

        Interview__c[] upsertInterviews = new List<Interview__c>();

        for (Interview__c inter : (Interview__c[]) setupData.get('interviews')) {
            inter.Completed__c = true;
            upsertInterviews.add(inter);
        }

        update upsertInterviews;

        Interview__c inter = (Interview__c) setupData.get('interviews')[0];
        inter.Candidate_hired__c = true;

        DmlException err;
        try {
            upsert inter;
        } catch (DmlException e) {
            err = e;
        }
        System.assert(err == null);

        for (Vacancy__c vac : [select id, Closed_Date__c, Status__c, Approved_candidate__c from Vacancy__c]) {
            System.debug(vac);
            System.assertNotEquals(vac.Closed_Date__c, null);
            System.assertEquals(vac.Status__c, 'Closed');
            System.assertNotEquals(vac.Approved_candidate__c, null);
            System.assertEquals(vac.Approved_candidate__c, inter.Interviewee__c);
        }
    }
}