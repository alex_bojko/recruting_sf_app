/**
 * Created by oboiko2 on 12/12/19.
 */

global class notifyFeedBacksBatchScheduler implements Schedulable, Database.Batchable<sObject> {
    @TestVisible static SObjectField dateField = FeedBack__c.LastModifiedDate;
    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('Batchable.START');
        String query = 'SELECT id, ' +
                'Interview__r.isClosed__c, '+
                'Interview__r.id, ' +
                'Interview__r.Vacancy__r.Status__c, ' +
                'Interview__r.Vacancy__r.recruter__r.Account.OwnerId, ' +
                'Interviewer__r.Id, ' +
                'Interviewer__r.Name, ' +
                'Interview__r.Owner.Id ' +
                'FROM FeedBack__c ' +
                'WHERE isCompleted__c=false ' +
                'and ' + dateField + ' < YESTERDAY ' +
                'and Interview__r.isClosed__c=false ' +
                'and Interview__r.Vacancy__r.Status__c != \'Closed\'';
        return Database.getQueryLocator(query);
    }

    global void finish(Database.BatchableContext bc) {}

    global void execute(Database.BatchableContext bc, List<FeedBack__c> scope) {
        // TODO: add map feedback interview, not create new task for interview that already has it.
        List<Id> alreadyNotifiedInterviews = new List<Id>();
        Map<id, Interview__c> interviewsByIds = new Map<Id, Interview__c>();
        Map<Id, FeedBack__c[]> interviewFeedBacksByIds = new Map<Id, FeedBack__c[]>();
        for (FeedBack__c fb : scope) {
            FeedBack__c[] fbList = interviewFeedBacksByIds.get(fb.Interview__r.id);
            if (fbList == null) {
                fbList = new List<FeedBack__c>();
            }
            fbList.add(fb);
            interviewFeedBacksByIds.put(fb.Interview__r.id, fbList);
            if (!interviewsByIds.containsKey(fb.Interview__r.id)) {
                interviewsByIds.put(fb.Interview__r.id, fb.Interview__r);
            }
        }

        Task[] tasks = new List<Task>();
        String envURL = System.URL.getSalesforceBaseURL().getHost() + '/';

        for(Id  interviewId: interviewFeedBacksByIds.keySet()){
            String reviewers = '';
            String fbs = '';
            for (FeedBack__c fb : interviewFeedBacksByIds.get(interviewId)) {
                fbs += 'http://' + envURL + fb.Id + ' ';
            }

            String msg = 'Please review that some' +
                    ' reviewers still not submitted feedbacks (' +
                    fbs + ')';

            Interview__c interview = interviewsByIds.get(interviewId);

            Task newTask = new Task(
                    Subject=msg,
                    WhoId=interview.Owner.Id,
                    OwnerId=interview.Vacancy__r.recruter__r.Account.OwnerId
            );
            tasks.add(newTask);
        }
        insert tasks;
    }

    global void execute(System.SchedulableContext sc) {
        Database.executeBatch(this);
    }
}