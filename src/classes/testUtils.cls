/**
 * Created by oboiko2 on 12/13/19.
 */

public virtual class testUtils {
    public static String getUID() {
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        return h.SubString(0,8);
    }

    static FeedBack__c completeFeedback(FeedBack__c feedBack) {
        feedBack.English_score__c = '9';
        feedBack.Hard_skills_level__c = 'Senior';
        feedBack.Hard_Skills_Score__c = '10';
        feedBack.Soft_skills_score__c = '9';
        feedBack.recalculateFormulas();
        return feedBack;
    }

    public static User createUser(Profile profile) {
        String uid = getUID();
        return new User(
                Alias = uid,
                Email= uid + '@myorg.com',
                EmailEncodingKey='UTF-8',
                LastName='Testing',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                ProfileId = profile.Id,
                TimeZoneSidKey='America/New_York',
                UserName= uid + '@myorg.com'
        );
    }

    public static FeedBack__c createFeedBack(
            Interview__c interview,
            Contact interviewer,
            Integer submitDaysAgo,
            Boolean isCompleted
    ) {
        FeedBack__c fb = new FeedBack__c(
                Interview__c=interview.id,
                Interviewer__c=interviewer.id,
                WritableDateTime__c = Datetime.now().addDays(-1 * submitDaysAgo)
        );
        if (isCompleted) {
            return completeFeedback(fb);
        }
        return fb;
    }

    public static Account createAccount(User user) {
        return new Account(
                Name='TestAccount' + getUID(),
                OwnerId=user.id
        );
    }

    public static Contact createContact(Account acc) {
        return new Contact(Account=acc, LastName=acc.Name + '_Contact_' + getUID(), AccountId=acc.id);
    }

    public static Vacancy__c createVacancy(Contact contact) {
        return new Vacancy__c(
                Status__c='Open',
                recruter__c=contact.id,
                Due_date__c=Datetime.now().addDays(10).date(),
                Primary_skills_level__c='Middle',
                Name='some vacancy'
        );
    }

    public static Interviewee__c createCandidate() {
        String uid = getUID();
        return new Interviewee__c(
                date_of_birth__c = Date.today(),
                email__c = 'some' + uid + '@email.com',
                first_name__c = 'Ivan',
                last_name__c = 'Ivanov',
                Minimum_Salary__c = 2000,
                phone__c = '0800900911',
                Primary_Skills__c = 'Python'
        );
    }

    public static Interview__c createInterview(Vacancy__c vacancy, Interviewee__c candidate) {
        return new Interview__c(
                start_date__c = Datetime.now().addDays(10),
                Vacancy__c = vacancy.id,
                Interviewee__c=candidate.id
        );
    }

    public static Map<string, SObject[]> setup(
            Integer contactsCount,
            Integer interviewCount,
            Integer feedbackDaysAgo,
            Integer oldFeedbacksCount,
            Integer newFeedbacksCount,
            Integer completedFeedbacksCount

    ) {
        List<Account> accts = new List<Account>();
        List<Contact> contacts = new List<Contact>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User u = createUser(p);
        insert u;

        for(Integer i=0; i<1; i++) { // create only one acc!!!
            accts.add(createAccount(u));
        }
        insert accts;

        for(Account acc : accts){
            for (Integer j=0; j < contactsCount; j++) {
                contacts.add(createContact(acc));
            }
        }
        insert contacts;
        Contact interviewer = contacts[0];

        Vacancy__c vacancy = createVacancy(contacts[0]);
        insert vacancy;

        Interviewee__c[] interviewees = new List<Interviewee__c>();
        for (Integer i = 0; i < interviewCount; i++) {
            interviewees.add(createCandidate());
        }
        insert interviewees;

        Interview__c[] interviews = new List<Interview__c>();
        for (Integer i = 0; i < interviewCount; i++) {
            interviews.add(createInterview(vacancy, interviewees[i]));
        }
        insert interviews;

        FeedBack__c[] feedBacks = new List<FeedBack__c>();
        FeedBack__c[] oldFeedBacks = new List<FeedBack__c>();
        FeedBack__c[] newFeedBacks = new List<FeedBack__c>();
        FeedBack__c[] completedFeedBacks = new List<FeedBack__c>();


        for (Interview__c interview: interviews) {
            for (Integer i = 0; i < oldFeedbacksCount; i++){
                FeedBack__c feedBack = createFeedBack(
                        interview=interview, interviewer=interviewer, feedbackDaysAgo, false
                 );
                feedBacks.add(feedBack);
                oldFeedBacks.add(feedBack);
            }
            for (Integer i = 0; i < newFeedbacksCount; i++){
                FeedBack__c feedBack = createFeedBack(interview=interview, interviewer=interviewer, 0, false);
                feedBacks.add(feedBack);
                newFeedBacks.add(feedBack);
            }
            for (Integer i = 0; i < completedFeedbacksCount; i++){
                FeedBack__c feedBack = createFeedBack(interview=interview, interviewer=interviewer, 0, true);
                feedBacks.add(feedBack);
                completedFeedBacks.add(feedBack);
            }
        }
        insert feedBacks;

        System.debug('Accounts' + [select Name from Account where Account.Name like 'TestAccount_%']);

        Map<String, SObject[]> toReturn = new Map<String, SObject[]>();
        toReturn.put('accounts', accts);
        toReturn.put('contacts', contacts);
        toReturn.put('interviewer', new List<SObject>{interviewer});
        toReturn.put('feedbacks', feedBacks);
        toReturn.put('oldFeedbacks', (List<SObject>) oldFeedbacks);
        toReturn.put('newFeedbacks', (List<SObject>) newFeedbacks);
        toReturn.put('completedFeedBacks', (List<SObject>) completedFeedBacks);
        toReturn.put('candidates', (List<SObject>) interviewees);
        toReturn.put('interviews', (List<SObject>) interviews);
        toReturn.put('vacancies', new List<Vacancy__c>{vacancy});
        return toReturn;
    }
}