/**
 * Created by oboiko2 on 12/12/19.
 */

@IsTest
private class notifyFeedBacksBatchSchedulerTest extends testUtils {
    // TODO: rename first letter upper case
    // TODO: split scheduler and batch

    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    static testMethod void executeBatch() {
        Integer oldFeedbacksCount = 3, newFeedbacksCount = 0, completedFeedbacksCount = 0;
        Map<String, SObject[]> setupData = setup(
                5,                    // contactsCount,
                1,                    // interviewCount,
                2,                    // feedbackDaysAgo,
                oldFeedbacksCount,    // oldFeedbacksCount,
                newFeedbacksCount,    // newFeedbacksCount,
                completedFeedbacksCount// completedFeedbacks
        );
        Task[] oldTasks = [SELECT id from Task];
        Vacancy__c vacancy = ((Vacancy__c[]) setupData.get('vacancies'))[0];
        FeedBack__c[] oldFeedBacks = (FeedBack__c[]) setupData.get('oldFeedbacks');

        Test.startTest();
        notifyFeedBacksBatchScheduler.dateField = FeedBack__c.WritableDateTime__c;
        notifyFeedBacksBatchScheduler batchExecutor = new notifyFeedBacksBatchScheduler();
        Task[] tasks = [select id from Task];
        System.assertEquals(tasks.size(), 0, 'Tasks exist before running batch!');
        Database.executeBatch(batchExecutor);
        Test.stopTest();

        Task[] newTasks = [SELECT id, WhoId, OwnerId, Subject from Task];
        System.assertNotEquals(newTasks.size(), oldTasks.size());

        // check that batch created 1 tasks
        System.assertEquals(newTasks.size(), 1);
        Account account = (Account) setupData.get('accounts')[0];
//        FeedBack__c[] oldFeedBacks = (FeedBack__c) setupData.get('oldFeedbacks');
        String envURL = System.URL.getSalesforceBaseURL().getHost() + '/';

        for(Task task : newTasks){
            // check that all tasks has Who
            Interview__c interview;

            String reviewers = '';
            String fbs = '';
            for (Integer i = 0; i < oldFeedBacks.size(); i++) {
                FeedBack__c fb = oldFeedBacks[i];
                reviewers += envURL + fb.Interviewer__r.Name + ', ';
                fbs += 'http://' + envURL + fb.Id + ' ';

                interview = fb.Interview__r;
            }

            String msg = 'Please review that some' +
                    ' reviewers still not submitted feedbacks (' +
                    fbs + ')';

            System.assertEquals(msg, task.Subject);
            System.assertEquals(task.OwnerId, account.OwnerId); // check that task is assigned to user
        }
    }

    static testMethod void testScheduleBatch() {

        Task[] oldTasks = [SELECT id from Task];

        Test.startTest();
        notifyFeedBacksBatchScheduler.dateField = FeedBack__c.WritableDateTime__c;
        notifyFeedBacksBatchScheduler batchExecutor = new notifyFeedBacksBatchScheduler();
        String jobId = System.schedule('ScheduledApexTest', CRON_EXP, batchExecutor);
        Test.stopTest();

        List<AsyncApexJob> jobsScheduled = [
                select Id, ApexClassID, ApexClass.Name, Status, JobType
                from AsyncApexJob
                where JobType = 'ScheduledApex'
        ];

        System.assertEquals(1, jobsScheduled.size(), 'expecting one scheduled job');
    }
}