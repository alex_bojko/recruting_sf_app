/**
 * Created by oboiko2 on 12/9/19.
 */

public with sharing class InterviewTriggerHandler {
    public static void run(Interview__c[] newInterviews) {
        Map<Id, Interview__c> oldInterviewsByIds = new Map<id, Interview__c>([
                SELECT
                        Id,
                        Vacancy__r.Status__c,
                        Candidate_hired__c,
                        Completed__c,
                        isClosed__c,
                        Interviewee__c,
                (SELECT Id FROM FeedBacks__r WHERE isCompleted__c <> false)
                FROM Interview__c
        ]);
        Vacancy__c[] updatedVacancies = new List<Vacancy__c>();
        for (Interview__c interview : newInterviews) {
            // make a map from next query out of for loop
            Interview__c oldInterview = oldInterviewsByIds.get(interview.id);
            System.debug(String.format('oldInterview.Completed__c={0} interview.Completed__c = {1} new.Closed = {2} oldInterview.FeedBacks__r.size={3}', new List<Object>{
                    oldInterview.Completed__c, interview.Completed__c, interview.isClosed__c, oldInterview.FeedBacks__r.size()
            }));
            InterviewTriggerHandler.checkInterviewCompletedBeforeClose(interview, oldInterview);

            Vacancy__c updatedVacancy;
            updatedVacancy = InterviewTriggerHandler.updateVacancyOnHireCandidate(interview, oldInterview);
            System.debug('updated vacancy ' + updatedVacancy);
            if (updatedVacancy != null && !updatedVacancies.contains(updatedVacancy)) {
                updatedVacancies.add(updatedVacancy);
            }
        }
        if (updatedVacancies.size() > 0) {
            update updatedVacancies;
        }
    }

    static Vacancy__c updateVacancyOnHireCandidate(Interview__c interview, Interview__c oldInterview) {
        Vacancy__c vacancy = oldInterview.Vacancy__r;
        vacancy.Status__c = 'Closed';
        vacancy.Closed_Date__c = Datetime.now();
        if (interview.Candidate_hired__c) {
            vacancy.Approved_candidate__c = interview.Interviewee__c;
        }
        return vacancy;
    }

    static void checkInterviewCompletedBeforeClose(Interview__c interview, Interview__c oldInterview) {
        if (!interview.Completed__c && interview.isClosed__c) {
            interview.addError('Could not change flag `Interview took place` when interview is closed!');
        }
        // Move to Closed status only if it is already in Completed status.
        if (!oldInterview.Completed__c && !interview.Completed__c && interview.isClosed__c) {
            interview.addError('Could not close not completed interview!');
        }

        if (interview.isClosed__c  && oldInterview.FeedBacks__r.size() < 3) {
            interview.addError('Could not close interview until it has 3 completed Feedbacks.');
        }
    }
}